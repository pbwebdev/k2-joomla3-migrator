<?php

require_once 'forceutf8/src/ForceUTF8/Encoding.php';

use \ForceUTF8\Encoding;

class Migrate
{
    const MYSQL_BIN = '/Applications/XAMPP/xamppfiles/bin/mysql';
    const SQL_DUMP_PATH = '/Applications/XAMPP/htdocs/joomla3/joomla3.sql';
    const AUTHOR_USER_ID = 42;
    const JOOMLA3_DB_NAME = 'freshjoomla';
    const JOOMLA3_DB_HOST = 'localhost';
    const JOOMLA3_DB_USER = 'root';
    const JOOMLA3_DB_PASS = 'secret';
    const JOOMLA3_DB_PREFIX = 'kgq4a_';
    const K2_DB_PREFIX = 'pbLIVE_';
    const K2_DB_NAME = 'hcourt';
    const K2_DB_HOST = 'localhost';
    const K2_DB_USER = 'root';
    const K2_DB_PASS = 'secret';

    private $k2_db;
    private $categories = array();

    public function __construct()
    {
        $this->k2_db = new PDO('mysql:dbname='.self::K2_DB_NAME.';host='.self::K2_DB_HOST, self::K2_DB_USER, self::K2_DB_PASS);
        $this->joomla3_db = new PDO('mysql:dbname='.self::JOOMLA3_DB_NAME.';host='.self::JOOMLA3_DB_HOST, self::JOOMLA3_DB_USER, self::JOOMLA3_DB_PASS);

        define( '_JEXEC', 1 );
        define( '_VALID_MOS', 1 );
        define( 'JPATH_BASE', realpath(dirname(__FILE__)));
        define( 'DS', DIRECTORY_SEPARATOR );
        require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
        require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );
        $mainframe =& JFactory::getApplication('site');
        $mainframe->initialise();
    }

    public function execute()
    {
        $this->reset_database();
        $this->migrate_categories();
        $this->migrate_items();
    }

    private function reset_database()
    {
        echo 'Confirm resetting database? y/n ';
        echo shell_exec(self::MYSQL_BIN.' -u'.self::JOOMLA3_DB_USER.' -p'.self::JOOMLA3_DB_PASS.' drop '.self::JOOMLA3_DB_NAME);
        echo shell_exec(self::MYSQL_BIN.' -u'.self::JOOMLA3_DB_USER.' -p'.self::JOOMLA3_DB_PASS.' create '.self::JOOMLA3_DB_NAME);
        echo shell_exec(self::MYSQL_BIN.' -u'.self::JOOMLA3_DB_USER.' -p'.self::JOOMLA3_DB_PASS.' '.self::JOOMLA3_DB_NAME.' < '.self::SQL_DUMP_PATH);
    }

    private function migrate_categories()
    {
        $categories = $this->k2_db->query('SELECT * FROM '.self::K2_DB_PREFIX.'k2_categories WHERE trash != 1')->fetchAll();

        foreach ($categories as & $category)
        {
            echo "\n\n".'Beginning transfer of category:';
            echo " ${category['name']}";
            $data = array(
                'extension' => 'com_content',
                'title' => $category['name'],
                'path' => $category['alias'],
                'alias' => $category['alias'].$category['id'],
                'description' => Encoding::toUTF8($category['description']),
                'published' => $category['published'],
                'params' => array(
                    'category_layout' => '',
                    'image' => ''
                ),
                'metadata' => array(
                    'author' => '',
                    'robots' => ''
                ),
                'created_user_id' => self::AUTHOR_USER_ID
            );
            // var_dump($data);
            $table = JTableCategory::getInstance('category', 'JTable');
            $table->bind($data);
            $table->store();
            $primary_keys = $table->getPrimaryKey();
            $this->categories[$category['id']] = $primary_keys['id'];
            $category['new_id'] = $primary_keys['id'];
        }

        foreach ($categories as $category)
        {
            if ($category['parent'])
            {
                $this->joomla3_db->query('UPDATE '.self::JOOMLA3_DB_PREFIX.'categories SET level='
                    .(int) $this->find_levels($categories, $category['parent'])
                    .', parent_id='
                    .$this->categories[$category['parent']]
                    .' WHERE id='
                    . (int) $category['new_id']);
            }
            else
            {
                $this->joomla3_db->query('UPDATE '.self::JOOMLA3_DB_PREFIX.'categories SET level=1, parent_id=1 WHERE id='. (int) $category['new_id']);
            }
        }
    }

    private function find_levels($categories, $parent_id, $current_level=1)
    {
        if ( ! $parent_id)
            return $current_level;

        $current_level++;
        foreach ($categories as $category)
        {
            if ($category['id'] != $parent_id)
                continue;

            return $this->find_levels($categories, $category['parent'], $current_level);
        }
    }

    private function migrate_items()
    {
        $items = $this->k2_db->query('SELECT * FROM '.self::K2_DB_PREFIX.'k2_items WHERE trash != 1')->fetchAll();
        foreach ($items as $item)
        {
            echo "\n\n".'Beginning transfer of item:';
            echo " ${item['title']}";
            $data = array(
                'title' => Encoding::toUTF8($item['title']),
                'alias' => Encoding::toUTF8($item['alias'].$item['id']),
                'catid' => $this->categories[$item['catid']],
                'state' => $item['published'],
                'publish_up' => $item['publish_up'],
                'publish_down' => $item['publish_down'],
                'introtext' => Encoding::toUTF8($item['introtext']),
                'fulltext' => Encoding::toUTF8($this->generate_fulltext($item)),
                'hits' => $item['hits'],
                'created_by' => self::AUTHOR_USER_ID
            );
            // var_dump($data);
            $table = JTableContent::getInstance('content', 'JTable');
            $table->bind($data);
            $table->store();
        }
    }

    private function generate_fulltext($item)
    {
        $fulltext = $item['fulltext'];

        $fulltext .= (empty($item['video'])) ? NULL : '<p>'.$item['video'].'</p>';
        $fulltext .= (empty($item['video_caption'])) ? NULL : '<p>'.$item['video_caption'].'</p>';
        $fulltext .= (empty($item['video_credits'])) ? NULL : '<p>'.$item['video_credits'].'</p>';
        $fulltext .= (empty($item['image'])) ? NULL : '<p>'.$item['image'].'</p>';
        $fulltext .= (empty($item['image_caption'])) ? NULL : '<p>'.$item['image_caption'].'</p>';
        $fulltext .= (empty($item['image_credits'])) ? NULL : '<p>'.$item['image_credits'].'</p>';

        return $fulltext;
    }
}

$migrate = new Migrate;
$migrate->execute();
