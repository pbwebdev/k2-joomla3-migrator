# k2-joomla3-migrator

Migrates content from K2 to Joomla3 native

# Instructions

 1. Place `migrate.php` in the base directory of the Joomla3 installation you want to import _to_.
 2. Dump the current state of the Joomla3 installation for the script to use.
 3. Modify the constants in `migrate.php` to the dumped Joomla3, and where the old K2 database is.
 4. Grab https://github.com/neitanod/forceutf8/
 5. `php migrate.php`
